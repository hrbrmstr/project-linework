﻿====================================================
Twenty Seventy: A linework set from Project Linework
====================================================

Linework drawn by Martin E. Elmer

Released to the public domain. Please credit the author and the project wherever possible.



REVISION HISTORY

Version 1.1 - 10/04/2013
---------------------
LAND: New shapefile/AI layer
ADMIN-1: Cleaned topology in shapefile
Added GeoJSON files
Added TopoJSON files


Version 1.0 — 3/21/13
---------------------
Initial release