Project Linework
================
This is the page for the Project Linework geo-files. The website can be found on the [gh-pages branch](https://github.com/svmatthews/project-linework/tree/gh-pages) of this repository. Download the current files at [projectlinework.org](http://projectlinework.org/). [Learn more](http://projectlinework.org/about/) about Mr. Huffman's inspiration.

> Project Linework is a library of handcrafted vector linework for cartography, each one designed in a unique aesthetic style. It’s meant to break us away from the default line paths that we so often rely on by providing some more visually-interesting alternatives.

##Linework Sets

* Charmingly Innacurate
* Elmer Casual
* Geo Metro
* Liana
* Times Approximate
* Twenty Seventy
* Wargames

##Download Packages

Linework file types:

* **Illustrator** CS3 Linework *(projected to best fit extent)*
* **Shapefiles**
* **GeoJSON**: ogr2ogr .shp to .json
* **Topojson**: quantization [magnitude of 1e5](http://www.projectlinework.org/2013/10/07/topojson_files/)
* **Readme**

Geographic Data (not included in all sets):

* Admin0 Poly/Line
* Admin1 Poly/Line
* Streams
* Lakes
* Waterbodies

##License

Released to the public domain. Please credit the author and the project wherever possible.
